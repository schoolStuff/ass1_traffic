globals [
  ruleSetSize ; size of expected ruleset, calculated from k and r
  ruleSet ; random rule set generated from lambda
  colorCodes ; color swatches to use for coloring each state list of length k.
  row ; row currently being operated on

  total-car-passed ; total number of cars that passed throught the right-hand side of window
  ]
patches-own [state] ; the state of cell 0-k

to setup
  ;housekeeping
  clear-all
  reset-ticks

  set ruleSetSize k ^ (2 * range + 1) ;size of the rule set for this CA

  if RuleNumber >= maxRuleNumber [ set RuleNumber maxRuleNumber - 1  ]  ; clip the rule number if too big
  set ruleSet buildRuleSet RuleNumber ;build the ruleset from the rule number

  ;set colors for different states (see menu Tools>Color Swatches)
  set colorCodes n-values k [85 + 10 * ? ] ; colors to use for states
  set colorCodes replace-item 0 colorCodes 0 ; always use black for state 0.

  ;set initial row to evolve from (top y patch coord)
  set row max-pycor

  ;Setup first row initial patches (initial conditions) and draw them
  setup-patches
  draw-patches

  set total-car-passed 0
end


to draw-patches
  ask patches with [pycor = row] [set pcolor (item state colorCodes)]
end


to go
  if (row = min-pycor) [ stop ] ; stop at the end
  ask patches with [pycor = row]
    [do-rule]

  update-car-passed ; updates the total number of cars passed every tick

  set row (row - 1)
  draw-patches
  tick
end

to do-rule
  let x-values n-values (2 * range + 1) [ ? - range ] ; range of neighboorhood xvalues
  let neighborStates n-values (2 * range + 1) [0]
  let indx 0
  foreach x-values
  [
    set neighborStates replace-item indx neighborStates [state] of patch-at ?1 0
    set indx indx + 1
  ]
  ask patch-at 0 -1 [set state (checkRule neighborStates)] ; set next row state
end


; reporter for calculating max rule number (total number of possible rule tables)
to-report maxRuleNumber
  report k ^ ruleSetSize
end


; takes a list lst assuming it represents a number in base k. Then returns the same number in base 10 (decimal)
to-report baseKtoDecimal [ lst ]
  ;length lst this should be 2*r + 1
  let dec 0 ; temporary variable to build up decimal value
  let index 0 ; current index in the list
  foreach lst ; loop through each item in the list and increment dec appropriately.
  [
    let exponent length lst - index - 1 ; exponent of current item
    set dec (?1 * k ^ (exponent) ) + dec ; increment dec by i * k ^ exponent
    set index index + 1 ; increment index
  ]
  report dec ; return the calculated decimal value
end


; takes n which is a base 10 (decimal) number and returns a list containing the eqivalent base k number.
to-report decimalTobaseK [n]
  let m n
  let baseK-list []
  while [m != 0]
  [
    set baseK-list fput (m mod k) baseK-list
    set m floor(m / k)
  ]
  report baseK-list
end

; builds a rules set from a specified decimal number n
; the output is a list of length k^2r+1 representing n (base 10) in base K.
to-report buildRuleSet [n]
  ; take the rule number specified at the interface and build a list containing all the outputs for each input
  ; recall that the standard is to specify the inputs in decreasing order
  ; so for k = 2 r = 1, we have a input alphabet size of 2^(2*1 +1) = 8
  ; we then have a list of size 8 to represent each output [0 1 1 1 0 1 1 1] would represent rule 119

  let baseKList decimalToBaseK n ; this is a list of length l < k^2r+1
  let l length baseKList ; get length of important part of output string

  let paddedZeros n-values ((k ^ (2 * range + 1)) - l) [0] ; this will create a list of k^(2r+1) - l zeros to pad our ouput string

  let ruleList sentence paddedZeros baseKList ; ruleList is our output string of states

  ;print ruleList
  report ruleList
end

to-report checkRule [input]
  ; input is a list representing the input of the automata, so something of length 2r+1
  ; each item in the list is a value between the range [0, k-1]
  ; the reporter should return (or report) the new state as defined by the ruleset we built inside the buildRuleSet reporter (see above)
  report item (ruleSetSize - (baseKtoDecimal input) - 1) ruleSet
end

to setup-patches
  ; first row of the CA is initialised to a random configuration with density specified in `initialRowDensity'
  let initial-state n-values (2 * max-pxcor) [0]

  let idx 0 ; index for counting
  foreach initial-state
  [
    if ((random 10 / 10) > (1 - initialRowDensity))
    [
      set initial-state replace-item idx initial-state (random (k - 1) + 1)
    ]
    set idx idx + 1
  ]
  ;print initial-state
  set idx 0
  foreach initial-state
  [
    ask patch (idx - 0.5 * length initial-state) row [set state ?1]
    set idx idx + 1
  ]
end


to update-car-passed
  ; checks and updates whether a car at the right end passes the window in the next time step
  let state0 [state] of patch max-pycor row ; state0 is the state of the patch on the right end
  if state0 = 1 ; if the current patch does have a car on it
  [
    let state-right [state] of patch min-pycor row
    ; state-right is the state of the patch to the right of the right-end one,
    ; underperiodic bdy conditions, this is the patch on the left end

    if state-right = 0 ; if there is no car in front blocking its way
    [
      set total-car-passed total-car-passed + 1 ; if the car does pass, update the total number
    ]
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
296
12
1307
644
500
300
1.0
1
10
1
1
1
0
1
0
1
-500
500
-300
300
1
1
1
ticks
30.0

BUTTON
22
107
122
140
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
22
143
122
176
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
150
14
290
47
k
k
2
5
2
1
1
NIL
HORIZONTAL

SLIDER
8
14
148
47
range
range
1
5
1
1
1
NIL
HORIZONTAL

INPUTBOX
151
52
290
112
RuleNumber
184
1
0
Number

MONITOR
8
51
148
96
NIL
maxRuleNumber
17
1
11

INPUTBOX
151
116
290
176
initialRowDensity
0.8
1
0
Number

PLOT
37
234
237
384
total number of cars passed
time
number of cars
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot total-car-passed"

@#$#@#$#@
## WHAT IS IT?

This program attempts to replicate the program of Chris Langton in Computation at the edge of chaos (http://dl.acm.org/citation.cfm?id=87520)

[1] Chris G. Langton. 1990. Computation at the edge of chaos: phase transitions and emergent computation. In Proceedings of the ninth annual international conference of the Center for Nonlinear Studies on Self-organizing, Collective, and Cooperative Phenomena in Natural and Artificial Computing Networks on Emergent computation (CNLS '89), Stephanie Forrest (Ed.). North-Holland Publishing Co., Amsterdam, The Netherlands, The Netherlands, 12-37.

The Langton lambda parameter is a single way to express behvaior of a cellular automata.
Pick an arbitrary state s ∈ S and call it the quiescent state sq
Count the number of rules in S that produce this particular quiescent state, and call it n
The other k^N-n transitions must produce the non-quiescent states of S - sq, but may otherwise be chosen at random.


## HOW TO USE IT

Initialization & Running:
- set a value of lambda between 0 and 1
- SETUP creates an initial row of randomly set cells and generates a random rule table according to lambda
- WALKTHROUGH specifies the algorithm to create the table, if on it uses the table-walk-through method [1] otherwise it uses the random table method (not implemented yet)
- GO begins running the model with the currently generate rule. It continues until the end of the view.

## THINGS TO NOTICE

As you vary lambda you will expect to observe different dynamics of the CA. High values of lambda generate chaotic not repeating patterns, whereas small values will typically generate CAs that reach steady state. Around values of 0.5 you should see interesting and complex behavior with long lasting transients.

## THINGS TO TRY

Try changing the dimensions of the world either to see more of the CA's pattern or to focus in on a region of interest.

What happens to the regularity when SETUP-CONTINUE is used a number of times?  Why do you suppose that is? (Note that in this model, the CA wraps around the sides.)

Is there any consistent pattern to the way this CA evolves?

If you look at a vertical line, are there more yellow or black cells?

Can you predict what the color of the nth cell on a line will be?

## EXTENDING THE MODEL

...TODO

## RELATED MODELS

...TODO

## CREDITS AND REFERENCES

Thanks to Uri Wilensky who's examples were extensively to help build this model.

The first cellular automaton was conceived by John Von Neumann in the late 1940's for his analysis of machine reproduction under the suggestion of Stanislaw M. Ulam. It was later completed and documented by Arthur W. Burks in the 1960's. Other two-dimensional cellular automata, and particularly the game of "Life," were explored by John Conway in the 1970's. Many others have since researched CA's. In the late 1970's and 1980's Chris Langton, Tom Toffoli and Stephen Wolfram did some notable research. Wolfram classified all 256 one-dimensional two-state single-neighbor cellular automata. In his recent book, "A New Kind of Science," Wolfram presents many examples of cellular automata and argues for their fundamental importance in doing science.

See also:

Von Neumann, J. and Burks, A. W., Eds, 1966. Theory of Self-Reproducing Automata. University of Illinois Press, Champaign, IL.

Toffoli, T. 1977. Computation and construction universality of reversible cellular automata. J. Comput. Syst. Sci. 15, 213-231.

Langton, C. 1984. Self-reproduction in cellular automata. Physica D 10, 134-144

Chris G. Langton. 1990. Computation at the edge of chaos: phase transitions and emergent computation. In Proceedings of the ninth annual international conference of the Center for Nonlinear Studies on Self-organizing, Collective, and Cooperative Phenomena in Natural and Artificial Computing Networks on Emergent computation (CNLS '89), Stephanie Forrest (Ed.). North-Holland Publishing Co., Amsterdam, The Netherlands, The Netherlands, 12-37.

Wolfram, S. 1986. Theory and Applications of Cellular Automata: Including Selected Papers 1983-1986. World Scientific Publishing Co., Inc., River Edge, NJ.

Bar-Yam, Y. 1997. Dynamics of Complex Systems. Perseus Press. Reading, Ma.

Wolfram, S. 2002. A New Kind of Science.  Wolfram Media Inc.  Champaign, IL.
See chapters 2 and 3 for more information on 1 Dimensional CAs
See index for more information specifically about Rule 110.

## COPYRIGHT AND LICENSE

Copyright 2013 Michael Lees.

![CC BY-NC-SA 3.0](http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png)

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License.  To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/ or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

Commercial licenses are also available. To inquire about commercial licenses, please contact Michael Lees m.h.lees@uva.nl
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.3.1
@#$#@#$#@
setup
repeat world-height - 1 [ go ]
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
