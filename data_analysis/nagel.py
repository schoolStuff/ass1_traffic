import csv
import numpy as np
from matplotlib import pyplot as plt

#with open('nagel-undersampled.csv', 'r') as f:
with open('nagel-final.csv', 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        try:
            if row[0] == '' and row[1] != '':
                data = row[1:] # mean-flow; avg-velocity
        except:
            pass


vmax = np.arange(3,6)
density = np.arange(.01, 1.01, .01)
probability = np.arange(0, .7, .3)
car_flow = (np.array([float(a) for a in data[::2]])).reshape((len(vmax), len(density),
    len(probability), -1))
v_avg = (np.array([float(v) for v in data[1::2]])).reshape((len(vmax), len(density),
    len(probability), -1))

for v in range(len(vmax)):
    plt.figure()
    for p in range(len(probability)):
        flow_array = [0,]
        rho_array = [0,]
        for rho in range(len(density)):
            rho_array.append(density[rho])
            flow_array.append(np.average(car_flow[v][rho][p][:]))
        plt.plot(rho_array, flow_array, linestyle='--', color='k', linewidth=1.5,
                marker='o', markerfacecolor=(.08,.53,probability[p]), markersize=4)
        plt.text(.51, flow_array[50]+.02, 'p = %s' % (probability[p]),
                color=(.08,.53,probability[p]), fontsize=10)
    plt.grid(True, which='both')
    plt.xlabel('Traffic density')
    plt.ylabel('Flow')
    plt.title('v$_{max} = $ %s' % (vmax[v]))
    #plt.savefig('nagel_v%s_undersampled.png' % (vmax[v]))
    plt.savefig('nagel_v%s.png' % (vmax[v]))
    plt.close()

for v in range(len(vmax)):
    plt.figure()
    for p in range(len(probability)):
        v_array = [0,]
        for rho in range(len(density)):
            v_array.append(np.average(v_avg[v][rho][p][:]))
        plt.plot(rho_array, v_array, linestyle='--', color='k', linewidth=1.5,
                marker='o', markerfacecolor=(probability[p],.40,.64), markersize=4)
        x_pos = -.6 * probability[p] + .8
        y_pos = v_array[int(x_pos*len(v_array))]
        plt.text(x_pos, y_pos, 'p = %s' % (probability[p]),
                color=(probability[p],.40,.64), fontsize=10)
    plt.grid(True, which='both')
    plt.xlabel('Traffic density')
    plt.ylabel('Average velocity')
    plt.title('v$_{max} = $ %s' % (vmax[v]))
    #plt.savefig('nagel_v%s_undersampled_v.png' % (vmax[v]))
    plt.savefig('nagel_v%s_v.png' % (vmax[v]))
    plt.close()

